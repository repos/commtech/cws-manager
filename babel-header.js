const pjson = require('./package.json');
console.log(
	'WARNING: GLOBAL GADGET FILE\n' +
	'Compiled from source at https://gitlab.wikimedia.org/repos/commtech/cws-manager\n\n' +
	'Script:         cws-manager.js\n' +
	`Version:        ${pjson.version}\n` +
	'Author:         MusikAnimal\n' +
	'License:        GPL-3.0-or-later\n' +
	'Source:         https://gitlab.wikimedia.org/repos/commtech/add-me'
);
