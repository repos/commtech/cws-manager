/* eslint-env node */
/* eslint-disable no-console */

const fs = require( 'fs' );
const { mwn } = require( 'mwn' );
const { execSync } = require( 'child_process' );

// JSON file with your username and bot password. Set up a bot password at [[Special:BotPasswords]]
const credentials = require( '../credentials.json' );

// Names of the on-wiki script pages
const MAIN_SCRIPT_PAGE = 'MediaWiki:Gadget-cws-manager.js';
const DIALOG_SCRIPT_PAGE = 'MediaWiki:Gadget-cws-manager-dialog.js';

/* eslint-disable-next-line new-cap */
const bot = new mwn( {
	apiUrl: 'https://meta.wikimedia.org/w/api.php',
	username: credentials.username,
	password: credentials.password
} );

// Version info for edit summary.
const sha = execSync( 'git rev-parse --short HEAD' ).toString();
const summary = process.argv[ 2 ] || execSync( 'git log -1 --pretty=%B' ).toString().trim();
const version = require( '../package.json' ).version;

bot.login().then( () => {
	const outjsMain = fs.readFileSync( __dirname + '/../src/cws-manager.js' ).toString();
	bot.save( MAIN_SCRIPT_PAGE, outjsMain, `v${version} at ${sha.trim()}: ${summary}` ).then( () => {
		console.log( `Successfully saved cws-manager.js to ${MAIN_SCRIPT_PAGE}` );
	}, ( e ) => {
		console.log( `Failed to edit ${MAIN_SCRIPT_PAGE}: ${e}` );
	} );

	const outjsDialog = fs.readFileSync( __dirname + '/../src/cws-manager-dialog.js' ).toString();
	bot.save( DIALOG_SCRIPT_PAGE, outjsDialog, `v${version} at ${sha.trim()}: ${summary}` ).then( () => {
		console.log( `Successfully saved cws-manager-dialog.js to ${DIALOG_SCRIPT_PAGE}` );
	}, ( e ) => {
		console.log( `Failed to edit ${DIALOG_SCRIPT_PAGE}: ${e}` );
	} );
} );
