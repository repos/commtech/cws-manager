(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }
function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }
function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }
function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i]; return arr2; }
function _iterableToArrayLimit(arr, i) { var _i = null == arr ? null : "undefined" != typeof Symbol && arr[Symbol.iterator] || arr["@@iterator"]; if (null != _i) { var _s, _e, _x, _r, _arr = [], _n = !0, _d = !1; try { if (_x = (_i = _i.call(arr)).next, 0 === i) { if (Object(_i) !== _i) return; _n = !1; } else for (; !(_n = (_s = _x.call(_i)).done) && (_arr.push(_s.value), _arr.length !== i); _n = !0); } catch (err) { _d = !0, _e = err; } finally { try { if (!_n && null != _i["return"] && (_r = _i["return"](), Object(_r) !== _r)) return; } finally { if (_d) throw _e; } } return _arr; } }
function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor); } }
function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }
function _get() { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get.bind(); } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(arguments.length < 3 ? target : receiver); } return desc.value; }; } return _get.apply(this, arguments); }
function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }
function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); Object.defineProperty(subClass, "prototype", { writable: false }); if (superClass) _setPrototypeOf(subClass, superClass); }
function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }
function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }
function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }
function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }
function _classPrivateMethodInitSpec(obj, privateSet) { _checkPrivateRedeclaration(obj, privateSet); privateSet.add(obj); }
function _checkPrivateRedeclaration(obj, privateCollection) { if (privateCollection.has(obj)) { throw new TypeError("Cannot initialize the same private elements twice on an object"); } }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
function _classPrivateMethodGet(receiver, privateSet, fn) { if (!privateSet.has(receiver)) { throw new TypeError("attempted to get private field on non-instance"); } return fn; }
var _getActionFieldset = /*#__PURE__*/new WeakSet();
var _getMoveFieldset = /*#__PURE__*/new WeakSet();
var _getUnarchiveFieldset = /*#__PURE__*/new WeakSet();
var _getCategoryInput = /*#__PURE__*/new WeakSet();
var _getArchiveFieldset = /*#__PURE__*/new WeakSet();
var _getApproveFieldset = /*#__PURE__*/new WeakSet();
var _getWatchFieldset = /*#__PURE__*/new WeakSet();
var _getTitleParts = /*#__PURE__*/new WeakSet();
/**
 * @class
 * @property {CwsManager} cwsManager
 * @property {mw.Title} proposalTitle
 * @property {boolean} watched
 * @property {string|null} action
 */
var Dialog = /*#__PURE__*/function (_OO$ui$ProcessDialog) {
  _inherits(Dialog, _OO$ui$ProcessDialog);
  var _super = _createSuper(Dialog);
  /**
   * @param {CwsManager} cwsManagerContext
   * @param {string} proposalPageName
   * @param {boolean} watched
   * @constructor
   */
  function Dialog(cwsManagerContext, proposalPageName, watched) {
    var _this;
    _classCallCheck(this, Dialog);
    _this = _super.call(this);
    _classPrivateMethodInitSpec(_assertThisInitialized(_this), _getTitleParts);
    _classPrivateMethodInitSpec(_assertThisInitialized(_this), _getWatchFieldset);
    _classPrivateMethodInitSpec(_assertThisInitialized(_this), _getApproveFieldset);
    _classPrivateMethodInitSpec(_assertThisInitialized(_this), _getArchiveFieldset);
    _classPrivateMethodInitSpec(_assertThisInitialized(_this), _getCategoryInput);
    _classPrivateMethodInitSpec(_assertThisInitialized(_this), _getUnarchiveFieldset);
    _classPrivateMethodInitSpec(_assertThisInitialized(_this), _getMoveFieldset);
    _classPrivateMethodInitSpec(_assertThisInitialized(_this), _getActionFieldset);
    _this.cwsManager = cwsManagerContext;
    _this.proposalTitle = mw.Title.newFromText(proposalPageName).getPrefixedText();
    _this.watched = watched;
    _this.action = null;

    // Add properties needed by the ES5-based OOUI inheritance mechanism.
    // This roughly simulates OO.inheritClass()
    Dialog.parent = Dialog["super"] = OO.ui.ProcessDialog;
    OO.initClass(OO.ui.ProcessDialog);
    Dialog["static"] = Object.create(OO.ui.ProcessDialog["static"]);
    Object.keys(Dialog).forEach(function (key) {
      Dialog["static"][key] = Dialog[key];
    });
    return _this;
  }

  /**
   * @param {...*} args
   * @override
   */
  _createClass(Dialog, [{
    key: "initialize",
    value: function initialize() {
      var _get2;
      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }
      (_get2 = _get(_getPrototypeOf(Dialog.prototype), "initialize", this)).call.apply(_get2, [this].concat(args));
      this.actionFieldset = _classPrivateMethodGet(this, _getActionFieldset, _getActionFieldset2).call(this);
      this.moveFieldset = _classPrivateMethodGet(this, _getMoveFieldset, _getMoveFieldset2).call(this);
      this.moveFieldset.toggle(false);
      this.archiveFieldset = _classPrivateMethodGet(this, _getArchiveFieldset, _getArchiveFieldset2).call(this);
      this.archiveFieldset.toggle(false);
      this.unarchiveFieldset = _classPrivateMethodGet(this, _getUnarchiveFieldset, _getUnarchiveFieldset2).call(this);
      this.unarchiveFieldset.toggle(false);
      this.approveFieldset = _classPrivateMethodGet(this, _getApproveFieldset, _getApproveFieldset2).call(this);
      this.approveFieldset.toggle(false);
      this.watchFieldset = _classPrivateMethodGet(this, _getWatchFieldset, _getWatchFieldset2).call(this);
      this.formPanel = new OO.ui.PanelLayout({
        padded: true,
        expanded: false
      });
      this.formPanel.$element.append(this.actionFieldset.$element, this.moveFieldset.$element, this.archiveFieldset.$element, this.unarchiveFieldset.$element, this.approveFieldset.$element, this.watchFieldset.$element);
      this.$body.append(this.formPanel.$element);
    }

    /**
     * Update the view based on which action was selected.
     *
     * @param {OO.ui.RadioOptionWidget} radioButton
     */
  }, {
    key: "onActionInputChange",
    value: function onActionInputChange(radioButton) {
      this.action = radioButton.data;
      this.moveFieldset.toggle(this.action === 'move');
      this.approveFieldset.toggle(this.action === 'approve');
      this.archiveFieldset.toggle(this.action === 'archive');
      this.unarchiveFieldset.toggle(this.action === 'unarchive');
      this.getActions().setAbilities({
        submit: true
      });
      this.updateSize();
    }

    /**
     * Show/hide the archive reason input, as applicable.
     *
     * @param {OO.ui.MenuOptionWidget} option
     */
  }, {
    key: "onArchiveReasonInputChange",
    value: function onArchiveReasonInputChange(option) {
      var wikitext = option.data.reason;
      if (wikitext.includes('$1')) {
        this.archivePromptFieldLayout.setLabel(option.data.prompt);
        this.archivePromptFieldLayout.toggle(true);
      } else {
        this.archivePromptFieldLayout.toggle(false);
      }
      if (option.data.note) {
        this.archiveReasonFieldLayout.setNotices([option.data.note]);
      } else {
        this.archiveReasonFieldLayout.setNotices([]);
      }
      this.updateSize();
    }

    /**
     * Get the fieldset for selecting an action on the proposal.
     *
     * @return {OO.ui.FieldsetLayout}
     * @private
     */
  }, {
    key: "getSetupProcess",
    value:
    /**
     * @param {Object} data
     * @return {OO.ui.Process}
     * @override
     */
    function getSetupProcess(data) {
      var _this2 = this;
      return _get(_getPrototypeOf(Dialog.prototype), "getSetupProcess", this).call(this, data).next(function () {
        _this2.getActions().setAbilities({
          submit: !!_this2.action
        });
      });
    }

    /**
     * @param {string} action
     * @return {OO.ui.Process}
     * @override
     */
  }, {
    key: "getActionProcess",
    value: function getActionProcess(action) {
      var _this3 = this;
      return _get(_getPrototypeOf(Dialog.prototype), "getActionProcess", this).call(this, action).next(function () {
        if (action === 'submit') {
          return _this3.cwsManager.submit(_this3.action, _this3.getFormData(_this3.action));
        }
        return _get(_getPrototypeOf(Dialog.prototype), "getActionProcess", _this3).call(_this3, action);
      }).next(function () {
        if (action === 'submit') {
          // Redirect to the proposal (which may be the same page).
          // In the case of moving proposals, we rely on redirects.
          window.location.replace(mw.util.getUrl(_this3.proposalTitle));
        }
        return _get(_getPrototypeOf(Dialog.prototype), "getActionProcess", _this3).call(_this3, action);
      });
    }

    /**
     * Get an Object containing all the data we need for submission.
     *
     * @param {string} action
     * @return {Object}
     */
  }, {
    key: "getFormData",
    value: function getFormData(action) {
      var data = {
        proposalTitle: this.proposalTitle,
        watchProposal: this.watchCheckbox.isSelected()
      };
      switch (action) {
        case 'move':
          return Object.assign(data, {
            newName: this.moveTitleInput.getValue(),
            newCategory: this.moveCategoryInput.getValue(),
            reason: this.moveReasonInput.getValue()
          });
        case 'archive':
          return Object.assign(data, {
            reason: this.archiveReasonInput.getMenu().findSelectedItem().data.reason,
            param: this.archivePromptInput.getValue(),
            comment: this.archiveTextarea.getValue(),
            // 'archive' also 'move' so we need the subpage title as well
            newName: _classPrivateMethodGet(this, _getTitleParts, _getTitleParts2).call(this)[1]
          });
        case 'unarchive':
          return Object.assign(data, {
            newName: this.unarchiveTitleInput.getValue(),
            newCategory: this.unarchiveCategoryInput.getValue(),
            reason: this.unarchiveReasonInput.getValue()
          });
        case 'approve':
        // Falls through, no extra data needed.
      }

      return data;
    }
  }]);
  return Dialog;
}(OO.ui.ProcessDialog);
function _getActionFieldset2() {
  var _classPrivateMethodGe = _classPrivateMethodGet(this, _getTitleParts, _getTitleParts2).call(this),
    _classPrivateMethodGe2 = _slicedToArray(_classPrivateMethodGe, 1),
    categoryName = _classPrivateMethodGe2[0];
  var moveRadio = new OO.ui.RadioOptionWidget({
    label: 'Rename proposal or move to a different category',
    data: 'move'
  });
  var archiveRadio = new OO.ui.RadioOptionWidget({
    label: 'Move to the archive',
    data: 'archive'
  });
  archiveRadio.toggle(categoryName !== 'Archive');
  var unarchiveRadio = new OO.ui.RadioOptionWidget({
    label: 'Unarchive proposal',
    data: 'unarchive'
  });
  unarchiveRadio.toggle(categoryName === 'Archive');
  var approveRadio = new OO.ui.RadioOptionWidget({
    label: 'Approve and prepare for translation',
    data: 'approve'
  });
  approveRadio.toggle(!['Archive', 'Larger suggestions', 'Untranslated'].includes(categoryName));
  var radioSelect = new OO.ui.RadioSelectWidget({
    items: [moveRadio, archiveRadio, unarchiveRadio, approveRadio]
  });
  radioSelect.connect(this, {
    choose: this.onActionInputChange
  });
  return new OO.ui.FieldsetLayout({
    label: 'Select action to take on this proposal',
    items: [radioSelect]
  });
}
function _getMoveFieldset2() {
  var _classPrivateMethodGe3 = _classPrivateMethodGet(this, _getTitleParts, _getTitleParts2).call(this),
    _classPrivateMethodGe4 = _slicedToArray(_classPrivateMethodGe3, 2),
    categoryName = _classPrivateMethodGe4[0],
    proposalName = _classPrivateMethodGe4[1];
  var moveFieldset = new OO.ui.FieldsetLayout({
    label: 'Move proposal'
  });
  this.moveTitleInput = new OO.ui.TextInputWidget({
    value: proposalName,
    indicator: 'required'
  });
  this.moveCategoryInput = _classPrivateMethodGet(this, _getCategoryInput, _getCategoryInput2).call(this, categoryName);
  this.moveReasonInput = new OO.ui.TextInputWidget();
  moveFieldset.addItems([new OO.ui.FieldLayout(this.moveTitleInput, {
    label: 'Proposal title:',
    align: 'top'
  }), new OO.ui.FieldLayout(this.moveCategoryInput, {
    label: 'Category:',
    align: 'top'
  }), new OO.ui.FieldLayout(this.moveReasonInput, {
    label: 'Optional comment for the logs:',
    align: 'top'
  })]);
  return moveFieldset;
}
function _getUnarchiveFieldset2() {
  var _classPrivateMethodGe5 = _classPrivateMethodGet(this, _getTitleParts, _getTitleParts2).call(this),
    _classPrivateMethodGe6 = _slicedToArray(_classPrivateMethodGe5, 2),
    categoryName = _classPrivateMethodGe6[0],
    proposalName = _classPrivateMethodGe6[1];
  var unarchiveFieldset = new OO.ui.FieldsetLayout({
    label: 'Unarchive proposal'
  });
  this.unarchiveTitleInput = new OO.ui.TextInputWidget({
    value: proposalName,
    indicator: 'required'
  });
  this.unarchiveCategoryInput = _classPrivateMethodGet(this, _getCategoryInput, _getCategoryInput2).call(this, categoryName);
  this.unarchiveReasonInput = new OO.ui.TextInputWidget();
  unarchiveFieldset.addItems([new OO.ui.Element({
    text: 'Unarchiving will remove the archive rationale, if present, ' + 'and move the proposal to the specified category.'
  }), new OO.ui.FieldLayout(this.unarchiveTitleInput, {
    label: 'Proposal title:',
    align: 'top'
  }), new OO.ui.FieldLayout(this.unarchiveCategoryInput, {
    label: 'Category:',
    align: 'top'
  }), new OO.ui.FieldLayout(this.unarchiveReasonInput, {
    label: 'Optional comment for the logs:',
    align: 'top'
  })]);
  return unarchiveFieldset;
}
function _getCategoryInput2(categoryName) {
  return new OO.ui.DropdownInputWidget({
    options: this.cwsManager.config.categories.concat(['Larger suggestions']).map(function (category) {
      return {
        data: category,
        label: category
      };
    }),
    value: categoryName
  });
}
function _getArchiveFieldset2() {
  var reasons = this.cwsManager.config.archive_reasons;
  var dropdownOptions = [];
  reasons.forEach(function (reason) {
    dropdownOptions.push(new OO.ui.MenuOptionWidget({
      data: reason,
      label: reason.display || reason.reason
    }));
  });
  var archiveFieldset = new OO.ui.FieldsetLayout({
    label: 'Archive proposal'
  });
  this.archiveReasonInput = new OO.ui.DropdownWidget({
    label: 'Select a rationale for archiving…',
    menu: {
      items: dropdownOptions
    }
  });
  this.archiveReasonInput.getMenu().on('select', this.onArchiveReasonInputChange.bind(this));
  this.archiveReasonFieldLayout = new OO.ui.FieldLayout(this.archiveReasonInput, {
    label: 'Rationale:',
    align: 'top'
  });
  this.archivePromptInput = new OO.ui.TextInputWidget({
    indicator: 'required'
  });
  this.archivePromptFieldLayout = new OO.ui.FieldLayout(this.archivePromptInput, {
    align: 'top'
  });
  this.archivePromptFieldLayout.toggle(false);
  this.archiveTextarea = new OO.ui.MultilineTextInputWidget({
    placeholder: 'Add any additional messaging here, which will be posted as a comment in the Discussion section.'
  });
  var formElements = [this.archiveReasonFieldLayout, this.archivePromptFieldLayout, new OO.ui.FieldLayout(this.archiveTextarea, {
    label: 'Optional additional comment (encouraged):',
    align: 'top'
  })];
  archiveFieldset.addItems(formElements);
  return archiveFieldset;
}
function _getApproveFieldset2() {
  var content = new OO.ui.HtmlSnippet('<p>This action will move the proposal to a new /Proposal subpage, ' + 'convert it to use the proposal template, and insert translate tags. ' + 'Afterwards, the &lt;translate&gt; tags will become visible in transclusions until ' + 'the page is marked for translation, so <strong>please act quickly</strong>. ' + 'Please first verify no use of &lt;tvar&gt; or other fixes to the translation page are needed.');
  return new OO.ui.FieldsetLayout({
    label: 'Approve proposal',
    content: [content]
  });
}
function _getWatchFieldset2() {
  var watchFieldset = new OO.ui.FieldsetLayout();
  this.watchCheckbox = new OO.ui.CheckboxInputWidget({
    selected: this.watched
  });
  var formElements = [new OO.ui.FieldLayout(this.watchCheckbox, {
    label: 'Watch proposal page',
    align: 'inline'
  })];
  watchFieldset.addItems(formElements);
  return watchFieldset;
}
function _getTitleParts2() {
  var categoryName = this.proposalTitle.split('/')[1];
  var titleRegex = new RegExp("^".concat(this.cwsManager.config.survey_root, "/").concat(categoryName, "/"));
  return [categoryName, this.proposalTitle.replace(titleRegex, '')];
}
_defineProperty(Dialog, "name", 'cwsManagerDialog');
_defineProperty(Dialog, "size", 'medium');
_defineProperty(Dialog, "title", 'Manage proposal');
_defineProperty(Dialog, "actions", [{
  action: 'submit',
  label: 'Submit',
  flags: ['primary', 'progressive']
}, {
  label: 'Cancel',
  flags: ['safe', 'close']
}]);
module.exports = Dialog;

},{}],2:[function(require,module,exports){
/**
* WARNING: GLOBAL GADGET FILE
* Compiled from source at https://gitlab.wikimedia.org/repos/commtech/cws-manager
* 
* Script:         cws-manager.js
* Version:        0.1.1
* Author:         MusikAnimal
* License:        GPL-3.0-or-later
* Source:         https://gitlab.wikimedia.org/repos/commtech/add-me
* 
**/
"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }
function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }
function _iterableToArrayLimit(arr, i) { var _i = null == arr ? null : "undefined" != typeof Symbol && arr[Symbol.iterator] || arr["@@iterator"]; if (null != _i) { var _s, _e, _x, _r, _arr = [], _n = !0, _d = !1; try { if (_x = (_i = _i.call(arr)).next, 0 === i) { if (Object(_i) !== _i) return; _n = !1; } else for (; !(_n = (_s = _x.call(_i)).done) && (_arr.push(_s.value), _arr.length !== i); _n = !0); } catch (err) { _d = !0, _e = err; } finally { try { if (!_n && null != _i["return"] && (_r = _i["return"](), Object(_r) !== _r)) return; } finally { if (_d) throw _e; } } return _arr; } }
function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }
function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }
function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }
function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }
function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }
function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i]; return arr2; }
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor); } }
function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }
function _classPrivateMethodInitSpec(obj, privateSet) { _checkPrivateRedeclaration(obj, privateSet); privateSet.add(obj); }
function _checkPrivateRedeclaration(obj, privateCollection) { if (privateCollection.has(obj)) { throw new TypeError("Cannot initialize the same private elements twice on an object"); } }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
function _classPrivateMethodGet(receiver, privateSet, fn) { if (!privateSet.has(receiver)) { throw new TypeError("attempted to get private field on non-instance"); } return fn; }
var _fetchPageInfo = /*#__PURE__*/new WeakSet();
var _moveProposal = /*#__PURE__*/new WeakSet();
var _parseProposal = /*#__PURE__*/new WeakSet();
// <nowiki>
/**
 * @class
 * @property {jQuery} $content
 * @property {mw.Api} api
 * @property {Object} config
 * @property {OO.ui.ProcessDialog} dialog
 * @property {Object} pageCache
 * @property {boolean} viewingCategoryPage
 */
var CwsManager = /*#__PURE__*/function () {
  /**
   * @constructor
   * @param {jQuery} $content
   */
  function CwsManager($content) {
    _classCallCheck(this, CwsManager);
    _classPrivateMethodInitSpec(this, _parseProposal);
    _classPrivateMethodInitSpec(this, _moveProposal);
    _classPrivateMethodInitSpec(this, _fetchPageInfo);
    this.$content = $content;
    this.api = new mw.Api();
    this.config = null;
    this.pageCache = {};
    this.viewingCategoryPage = !!this.$content.find('.community-wishlist-edit-proposal-btn').length;
  }

  /**
   * Adds a "Manage" button to proposal headings.
   */
  _createClass(CwsManager, [{
    key: "addButtons",
    value: function addButtons() {
      var _this = this;
      // A little styling for button/text; only CSS this gadget should need.
      mw.util.addCSS('.cws-manager-btn { margin: 0 12px; }' + '.cws-manager-btn-approved .oo-ui-labelElement-label { color: #14866d }');
      var $existingButtons = this.viewingCategoryPage ? this.$content.find('.community-wishlist-edit-proposal-btn') : this.$content.find('.community-wishlist-proposal-back-btn');
      var proposalTitles = [];
      var parseProposalTitle = function parseProposalTitle(button) {
        return _this.viewingCategoryPage ? decodeURIComponent(button.parentElement.href.match(/(Community_Wishlist_Survey_\d+\/.*?\/.*?)&/)[1]) : mw.config.get('wgPageName');
      };

      // First collect all the proposal titles.
      $existingButtons.each(function (_i, button) {
        proposalTitles.push(parseProposalTitle(button));
      });

      // Build the page cache, then add the buttons where necessary.
      this.fetchPageInfo(proposalTitles).then(function () {
        // Now loop through again and add the buttons as necessary.
        $existingButtons.each(function (_i, button) {
          // Page cache uses normalized page titles (no underscores).
          var proposalTitle = parseProposalTitle(button).replaceAll('_', ' ');
          var newButton;
          if (_this.pageCache[proposalTitle] && _this.pageCache[proposalTitle].approved) {
            // Already approved; indicate as such with a link to the translation subpage.
            newButton = new OO.ui.ButtonWidget({
              label: 'Approved',
              flags: ['success'],
              icon: 'check',
              classes: ['cws-manager-btn cws-manager-btn-approved'],
              href: mw.util.getUrl("".concat(proposalTitle, "/Proposal")),
              title: 'This proposal has been approved and cannot managed further.'
            });
          } else {
            // Approve button
            newButton = new OO.ui.ButtonWidget({
              label: 'Manage',
              flags: ['primary', 'progressive'],
              classes: ['cws-manager-btn']
            });
            // Add click listener.
            newButton.on('click', function () {
              _this.fetchConfig().then(function () {
                _this.showDialog(proposalTitle, _this.pageCache[proposalTitle].watched);
              });
            });
          }
          $(button).parents('.plainlinks').append(newButton.$element);
        });
      });
    }

    /**
     * Show a Dialog for the given proposal
     *
     * @param {string} proposalTitle
     * @param {boolean} watched
     */
  }, {
    key: "showDialog",
    value: function showDialog(proposalTitle, watched) {
      var Dialog = require('./cws-manager-dialog.js');
      var windowManager = OO.ui.getWindowManager();
      if (!this.dialog) {
        this.dialog = new Dialog(this, proposalTitle, watched);
        windowManager.addWindows([this.dialog]);
      }
      windowManager.openWindow(this.dialog);
    }

    /**
     * Fetches and stores the config.
     *
     * @return {jQuery.Deferred}
     */
  }, {
    key: "fetchConfig",
    value: function fetchConfig() {
      var _this2 = this;
      var dfd = $.Deferred();
      if (this.config) {
        // Already loaded.
        return dfd.resolve();
      }
      this.api.get({
        action: 'query',
        prop: 'revisions',
        titles: CwsManager.configPage,
        rvprop: 'content',
        rvslots: 'main',
        format: 'json',
        formatversion: 2
      }).then(function (resp) {
        _this2.config = JSON.parse(resp.query.pages[0].revisions[0].slots.main.content);
        dfd.resolve();
      });
      return dfd;
    }

    /**
     * Get basic info about the proposal page and existence of its translation subpage.
     *
     * @param {string|Array<string>} proposalTitles
     * @return {jQuery.Deferred}
     */
  }, {
    key: "fetchPageInfo",
    value: function fetchPageInfo(proposalTitles) {
      var _this3 = this;
      var dfd = $.Deferred();
      if (!Array.isArray(proposalTitles)) {
        proposalTitles = [proposalTitles];
      }
      var promises = [];

      // First add /Proposal for each proposal title.
      proposalTitles.forEach(function (proposalTitle) {
        proposalTitles.push("".concat(proposalTitle, "/Proposal"));
      });

      // Break up proposal API requests into 50-title chunks.
      _toConsumableArray(Array(Math.ceil(proposalTitles.length / 50))).map(function () {
        return proposalTitles.splice(0, 50);
      }).forEach(function (chunk) {
        promises.push(_classPrivateMethodGet(_this3, _fetchPageInfo, _fetchPageInfo2).call(_this3, chunk));
      });
      Promise.all(promises).then(function (resp) {
        if (!resp || !resp[0] || !resp[0].query) {
          return dfd.reject();
        }
        resp[0].query.pages.forEach(function (page) {
          var normalizedTitle = page.title.replace(/\/Proposal$/, '');
          _this3.pageCache[normalizedTitle] = {};
          if (page.title.endsWith('/Proposal') && !page.missing) {
            _this3.pageCache[normalizedTitle].approved = true;
          } else if (!page.title.endsWith('/Proposal') && page.watched) {
            _this3.pageCache[normalizedTitle].watched = true;
          }
        });
        dfd.resolve();
      });
      return dfd;
    }

    /**
     * @param {Array<string>} proposalTitles
     * @return {jQuery.Promise}
     * @private
     */
  }, {
    key: "submit",
    value:
    /**
     * Called when the Dialog form is submitted.
     *
     * @param {string} action
     * @param {Object} data
     * @return {jQuery.Deferred}
     */
    function submit(action, data) {
      var dfd = $.Deferred();
      this["".concat(action, "Proposal")](data).fail(function (code, resp) {
        // Catch the 'articleexists' error, which means we need sysop intervention.
        if (action === 'move' && code === 'articleexists' && resp.error) {
          return dfd.reject(new OO.ui.Error("The target page \"".concat(data.newTitle, "\" already exists! If this is the correct page you intended to move to, ") + 'it probably means you need to request an admin to delete it first to make way for the move.', {
            recoverable: false
          }));
        }

        // Transform any other API errors into an OOUI Error.
        if (resp && resp.error && resp.error.info) {
          return dfd.reject(new OO.ui.Error(resp.error.info));
        }
        return dfd.reject(code, resp);
      }).then(function () {
        return dfd.resolve();
      });
      return dfd;
    }

    /**
     * Move a proposal page
     *
     * @param {Object} data
     * @return {jQuery.Promise}
     */
  }, {
    key: "moveProposal",
    value: function moveProposal(data) {
      var _this4 = this;
      data.newTitle = "".concat(this.config.survey_root, "/").concat(data.newCategory, "/").concat(data.newName);
      if (data.proposalTitle !== data.newTitle) {
        // The proposal name changed, so we need to first update the proposal header template.
        return this.api.edit(data.proposalTitle, function (revision) {
          var newContent = revision.content.replace(/^{{\s*Community Wishlist Survey\/Proposal header\s*\|\s*1=\s*(.*?)\s*}}/, "{{Community Wishlist Survey/Proposal header|1=".concat(data.newName, "}}"));

          // In this case we want to also purge the category page, even though this may
          // not finish before the page reloads (a purge may likely be needed, anyway).
          if (!data.proposalTitle.includes(data.newCategory)) {
            _this4.api.post({
              action: 'purge',
              titles: "".concat(_this4.config.survey_root, "/").concat(data.newCategory)
            });
          }
          return {
            text: newContent,
            summary: "Correcting proposal header template for [[".concat(data.newTitle, "|").concat(data.newName, "]]")
          };
        }).then(_classPrivateMethodGet(this, _moveProposal, _moveProposal2).bind(this, data));
      }
      return _classPrivateMethodGet(this, _moveProposal, _moveProposal2).call(this, data);
    }

    /**
     * @param {Object} data
     * @return {jQuery.Promise}
     * @private
     */
  }, {
    key: "archiveProposal",
    value:
    /**
     * Move a proposal to the Archive.
     *
     * @param {Object} data
     * @return {jQuery.Promise}
     */
    function archiveProposal(data) {
      var _this5 = this;
      if (data.reason.includes('$1') && data.param) {
        data.reason = data.reason.replace('$1', data.param);
      }
      var declineComment = "{{cross}} '''".concat(data.reason, "'''");

      // First add the decline comment and supplementary comment to the page.
      return this.api.edit(data.proposalTitle, function (revision) {
        // Add the decline rationale to the top.
        revision.content = revision.content.replace(/<!-- DO NOT EDIT ABOVE THIS LINE.*?\n+/, "<!-- DO NOT EDIT ABOVE THIS LINE -- DO NOT EDIT ABOVE THIS LINE -->\n".concat(declineComment, "\n\n"));
        // Remove extraneous new lines from the end.
        revision.content = revision.content.replace(/\n+$/, '\n');
        // Add the comment, if provided, at the bottom.
        if (data.comment) {
          data.comment = data.comment.trim().replace('~~~~', '');
          revision.content += "\n* ".concat(data.comment, " ~~~~\n");
        }
        // Return mutated content to edit API plugin.
        return {
          text: revision.content,
          summary: data.reason
        };
      }).then(function () {
        // Then move the proposal.
        return _this5.moveProposal(Object.assign(data, {
          newName: data.newName,
          newCategory: 'Archive'
        }));
      });
    }

    /**
     * Move a proposal out of the archives, removing the standardized archive
     * rationale that may have been added with this.archiveProposal().
     *
     * @param {Object} data
     * @return {jQuery.Promise}
     */
  }, {
    key: "unarchiveProposal",
    value: function unarchiveProposal(data) {
      var _this6 = this;
      return this.api.edit(data.proposalTitle, function (revision) {
        var text = revision.content.replace(/<!-- DO NOT EDIT ABOVE THIS LINE.*?\n{{cross}}.*?\n/, '<!-- DO NOT EDIT ABOVE THIS LINE -- DO NOT EDIT ABOVE THIS LINE -->\n');
        return {
          text: text,
          summary: 'Unarchiving proposal'
        };
      }).then(function () {
        return _this6.moveProposal(data);
      });
    }

    /**
     * Parse the proposal to use the Proposal template, add translate tags,
     * and move it to the /Proposal subpage.
     *
     * @param {Object} data
     * @return {jQuery.Deferred}
     */
  }, {
    key: "approveProposal",
    value: function approveProposal(data) {
      var _this7 = this;
      var dfd = $.Deferred();
      this.api.edit(data.proposalTitle, function (revision) {
        var _classPrivateMethodGe = _classPrivateMethodGet(_this7, _parseProposal, _parseProposal2).call(_this7, revision.content),
          _classPrivateMethodGe2 = _slicedToArray(_classPrivateMethodGe, 2),
          templateParams = _classPrivateMethodGe2[0],
          errorFields = _classPrivateMethodGe2[1];
        if (errorFields.length) {
          var fieldsList = errorFields.map(function (error) {
            return "'".concat(error, "'");
          }).join(', ');
          return dfd.reject(new OO.ui.Error("The following fields could not be parsed and require manual review: ".concat(fieldsList, ". ") + 'Please make sure the wikitext is in the expected format.'));
        }
        var subpageContent = ('<noinclude><languages/></noinclude>' + '{{:{{TNTN|Community Wishlist Survey/Proposal|uselang={{int:lang}}}}' + "\n| title = <translate>".concat(templateParams.title, "</translate>") + "\n| problem = <translate>".concat(templateParams.problem, "</translate>") + "\n| solution = <translate>".concat(templateParams.solution, "</translate>") + "\n| beneficiaries = <translate>".concat(templateParams.beneficiaries, "</translate>") + "\n| comments = <translate>".concat(templateParams.comments, "</translate>") + "\n| phab = ".concat(templateParams.phab) + "\n| proposer = ".concat(templateParams.proposer) + '\n| titleonly = {{{titleonly|}}}' + '\n}}'
        // Remove any empty translate tags.
        ).replaceAll('<translate></translate>', '');
        var subpageTitle = mw.Title.newFromText(data.proposalTitle + '/Proposal');

        // Create the /Proposal subpage.
        return _this7.api.create(subpageTitle, {
          summary: 'Creating translation subpage'
        }, subpageContent).then(function () {
          // Now that the /Proposal subpage is created, open it in a new tab.
          window.open(subpageTitle.getUrl({
            action: 'edit'
          }), '_blank');

          // Remove content from original page.
          var text = revision.content.replace(/<!\x2D\x2D DO NOT EDIT ABOVE THIS LINE[\s\S]*?\x2D\x2D>[\s\S]*<!\x2D\x2D DO NOT EDIT BELOW THIS LINE[\s\S]*?\x2D\x2D>/, '<!-- DO NOT EDIT ABOVE THIS LINE! PROPOSAL CONTENT IS NOW ON THE /Proposal ' + 'SUBPAGE (FOR TRANSLATION) AND SHOULD NOT BE MODIFIED FURTHER -->');

          // Return the new content for the original page to the edit API plugin.
          return {
            text: text,
            summary: "Moving proposal content to [[".concat(data.proposalTitle, "/Proposal]] for translation")
          };
        });
      }).then(function () {
        return dfd.resolve();
      });
      return dfd;
    }

    /**
     * Parse each field and the title from the proposal content.
     *
     * @param {string} content
     * @return {Array} [ template parameters, erroneous fields ]
     * @private
     */
  }, {
    key: "addTranslationHack",
    value:
    /**
     * Hacks into Special:PageTranslation by doing the following:
     * - If marking a proposal page for translation, the "Allow translation of page title"
     *   option is unchecked.
     * - After submission, the proposal page is added to the aggregate group
     *
     * This method expects the aggregate group to have a name of the form:
     *    agg-Community_Wishlist_Survey_2023_Proposals
     * replacing '2023' with the appropriate year.
     */
    function addTranslationHack() {
      var sessionKey = 'cws-translation-marked',
        matches = mw.config.get('wgPageName').match(/Community_Wishlist_Survey_(\d+)\/.*\/Proposal$/) || [],
        encodedMatches = location.href.match(/Special:PageTranslation.*?target=(.*?)&/);
      if (encodedMatches) {
        // We're at the index.php variant of the page.
        matches[0] = new URL(location.href).searchParams.get('target');
        matches[1] = matches[0].match(/^Community Wishlist Survey (\d+)/)[1];
      }

      // If there's a match, we're currently at Special:PageTranslation for a proposal.
      if (matches.length) {
        // Don't need to translate the page display title.
        // eslint-disable-next-line no-jquery/no-global-selector
        $('[name=translatetitle]').prop('checked', false);

        // Now set a flag in sessionStorage so that we can do more things post-submission.
        mw.storage.session.setObject(sessionKey, {
          page: matches[0].replaceAll('_', ' '),
          surveyYear: matches[1]
        });

        // Return early so the below code doesn't run :)
        return;
      }

      // From here we are at Special:PageTranslation, most likely post-submission. If the group
      // name is in session storage, we know we need to add it to the aggregate group.
      var groupInfo = mw.storage.session.getObject(sessionKey);
      if (groupInfo) {
        // Silently make a null edit (forcelinkupdate) to /Proposal to fix categorization.
        this.api.post({
          action: 'purge',
          titles: groupInfo.page,
          forcelinkupdate: 1
        });

        // Now add the page to the aggregate group.
        mw.storage.session.remove(sessionKey);
        var aggGroupName = "agg-Community_Wishlist_Survey_".concat(groupInfo.surveyYear, "_Proposals"),
          aggGroupNameDisplay = aggGroupName.replaceAll('_', ' ');
        this.api.postWithToken('csrf', {
          action: 'aggregategroups',
          aggregategroup: aggGroupName,
          "do": 'associate',
          group: "page-".concat(groupInfo.page)
        }).done(function () {
          mw.notify("\"".concat(groupInfo.page, "\" successfully added to the aggregate group \"").concat(aggGroupNameDisplay, "\""), {
            title: 'CWS Manager',
            type: 'success',
            autoHideSeconds: 'long'
          });
        }).fail(function (_, errObj) {
          mw.notify("Failed to add \"".concat(groupInfo.page, "\" to the aggregate group \"").concat(aggGroupNameDisplay, "\". ") + 'Check the console log for more information.', {
            title: 'CWS Manager',
            autoHide: false,
            type: 'error'
          });
          mw.log.warn("[CWS Manager] ".concat(errObj.error.info));
        });
      }
    }
  }]);
  return CwsManager;
}();
/**
 * Entry point, called after the 'wikipage.content' hook is fired.
 *
 * @param {jQuery} $content
 */
function _fetchPageInfo2(proposalTitles) {
  return this.api.get({
    action: 'query',
    prop: 'info',
    titles: proposalTitles.join('|'),
    inprop: 'watched',
    formatversion: 2
  });
}
function _moveProposal2(data) {
  return this.api.postWithToken('csrf', {
    action: 'move',
    from: data.proposalTitle,
    to: data.newTitle,
    reason: data.reason || '',
    watchlist: data.watchProposal ? 'watch' : 'preferences'
  });
}
function _parseProposal2(content) {
  var fields = {
    problem: 'Problem',
    solution: 'Proposed solution',
    beneficiaries: 'Who would benefit',
    comments: 'More comments',
    phab: 'Phabricator tickets',
    proposer: 'Proposer'
  };
  var templateParams = {};
  var errorFields = [];
  var fieldKeys = Object.keys(fields);
  fieldKeys.forEach(function (key, i) {
    var regex = new RegExp("'''\\s*".concat(fields[key], "\\s*'''\\s*:\\s*(.*?)\\s*\\n\\*\\s*'''\\s*").concat(fields[fieldKeys[i + 1]]), 's');
    if (key === 'proposer') {
      regex = /'''\s*Proposer\s*'''\s*:\s*(.*)\s*\n/;
    }
    var matches = content.match(regex);
    if (!matches) {
      errorFields.push(fields[key]);
      return;
    }
    var value = matches[1];
    if (key === 'phab') {
      // Remove comments.
      value = value.replace(/<!--.*?-->/, '');
    }
    templateParams[key] = value;
  });

  // Parse out the title
  templateParams.title = content.match(/Proposal header\|1=(.*?)}}/)[1];
  return [templateParams, errorFields];
}
_defineProperty(CwsManager, "configPage", 'User:Community_Tech_bot/WishlistSurvey/config');
function init($content) {
  Promise.all([
  // Resource loader modules
  mw.loader.using(['oojs-ui', 'mediawiki.util', 'mediawiki.api', 'mediawiki.Title']),
  // Page ready
  $.ready]).then(function () {
    // Only run on CWS pages and for WMF staff.
    if (/^Community_Wishlist_Survey_(\d+)\//.test(mw.config.get('wgPageName')) && /\s\(WMF\)$|-WMF$/.test(mw.config.get('wgUserName')) && !!$content.find('.community-wishlist-proposal-header')) {
      var cwsArchiver = new CwsManager($content);
      cwsArchiver.addButtons();
    }

    // For translation admins (okay if this is used by non-staff).
    if (mw.config.get('wgCanonicalSpecialPageName') === 'PageTranslation') {
      var _cwsArchiver = new CwsManager($content);
      _cwsArchiver.addTranslationHack();
    }

    // Remove listener so that CwsManager is only instantiated once.
    mw.hook('wikipage.content').remove(init);
  });
}
mw.hook('wikipage.content').add(init);
// </nowiki>

},{"./cws-manager-dialog.js":1}]},{},[2]);
