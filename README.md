# CWS Manager

A gadget used by WMF staff to (un)archive proposals in the [Community Wishlist Survey](https://meta.wikimedia.org/wiki/Community_Wishlist_Survey).

## Usage

See https://meta.wikimedia.org/wiki/Community_Wishlist_Survey/Staff_instructions#CWS_Manager_gadget

## Contributing

* Ensure you're using the Node version specified by .nvmrc
* `npm install` to install dependencies
* `npm test` to run linting tests

While developing, you can load the compiled code served from your local to the wiki.
To do this, add the following to your [global.js](https://meta.wikimedia.org/wiki/Special:MyPage/global.js):

```
mw.loader.load('http://localhost:5501/dist/cws-manager.js');
```

Then start the server with `npm run server`.

You can use `npm run watch` to recompile the source files as changes are made to them.

## Deployment

You must have `interface-admin` rights to use the deploy script.
Visit https://meta.wikimedia.org/wiki/Special:BotPasswords to obtain credentials,
then `cp credentials.json.dist credentials.json` and change the details accordingly:

```
{
   "username": "Exampleuser@somedescription",
   "password": "mybotpassword1234567890123456789"
}
```

To deploy, run `node bin/deploy.js "[edit summary]"`.
The edit summary is transformed to include the version number and git SHA, e.g. "v5.5.5 at abcd1234: [edit summary]".
